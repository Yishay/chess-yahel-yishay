#include "Player.h"
#include "Pawn.h"
#include "King.h"
#include "Bishop.h"
#include "Knight.h"
#include "Queen.h"
#include "Rook.h"
#include <string>
#include "define_it.h"

//  Constructor for the player -> create player with 16 pieces and initialized all by his color that got
Player::Player(int color)
{
	int i = 0;
	int j = 0;
	int d = NUM_OF_PAWN;
	char c1;
	char c2;
	std::string pPlace = "  ";//inserting at each player(black or white) his pieces by order)
	if (color == WHITE)//white:
	{
		this->p[i] = new Rook;
		i++;
		this->p[i] = new Knight;
		i++;
		this->p[i] = new Bishop;
		i++;
		this->p[i] = new King;
		i++;
		this->p[i] = new Queen;
		i++;
		this->p[i] = new Bishop;
		i++;
		this->p[i] = new Knight;
		i++;
		this->p[i] = new Rook;
		i++;
		for (i; i < NUM_OF_PIECES; i++)
		{
			this->p[i] = new Pawn;
		}

		i = 0;//insert to each pieces its place in board:
		for (j = 0; j + j * i < NUM_OF_PIECES; j++)
		{
			if (j == NUM_OF_PAWN)
			{
				i++;
				j = 0;
			}
			c1 = 'a' + j;
			c2 = i + '1';
			pPlace[0] = c1;
			pPlace[1] = c2;
			this->p[d*i + j]->setPlace(pPlace);
			this->p[d*i + j]->setColor("WHITE");

		}

	}
	else//black
	{
		for (i = 0; i < NUM_OF_PAWN; i++)
		{
			this->p[i] = new Pawn;
		}
		this->p[i] = new Rook;
		i++;
		this->p[i] = new Knight;
		i++;
		this->p[i] = new Bishop;
		i++;
		this->p[i] = new King;
		i++;
		this->p[i] = new Queen;
		i++;
		this->p[i] = new Bishop;
		i++;
		this->p[i] = new Knight;
		i++;
		this->p[i] = new Rook;
		i++;


		i = 0;
		for (j = 0; j + j * i < NUM_OF_PIECES; j++)
		{
			if (j == NUM_OF_PAWN)
			{
				i = 1;
				j = 0;
			}
			c1 = 'a' + j;
			c2 = i + '7';
			pPlace[0] = c1;
			pPlace[1] = c2;
			this->p[d*i + j]->setPlace(pPlace);
			this->p[d*i + j]->setColor("BLACK");


		}
	}
}

// destructor for the player -> delete his 16 pieces array then the player himself.
Player::~Player()
{
	for (int i = 0; i < 16; i++)
	{
		p[i]->~piece();
	}
}

// function will getting its place on the board and search for the piece in the pieces array and return him
piece& Player::find_pices_in_player(std::string move)
{
	int i = 0;
	std::string mov = "";
	mov = mov + move[0] + move[1];
	for (i = 0; i < 16; i++)
	{
		piece& s = *this->p[i];
		if (s.getPlace() == mov)
		{
			return *p[i];
		}
	}
}

// set a place for specific piece in the pieces array of the player by index (n)
std::string Player::getPlace(int n) const
{
	return p[n]->getPlace();
}

// get a place for specific piece in the pieces array of the player by index (n)
piece* Player::getPiece(int n)
{
	return p[n];
}