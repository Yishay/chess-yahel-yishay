#include "King.h"
#include"piece.h"
#include "define_it.h"

// Constructor for the King
King::King():piece()
{
}

// virtual destructor for the King
King::~King()
{
}

/*
Function will check if this king can do the move that got
by check if he can do it with his movement skills and if no one interrupt him.
Input: move to check and the board that help us to see things and check the move.
Output:
6 for INVALID_MOVE_INT - can't move this step.
or what ret got from the help function dont_interapt
*/
int King::can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE])
{
	std::string place1 = "";
	place1 = place1 + move[0] + move[1];
	std::string place2 = "";
	place2 = place2 + move[2] + move[3];
	if ((place2[1] == (place1[1] + 1) && place2[0] == place1[0]) || (place2[0] == (place1[0] - 1) && place2[1] == (place1[1]+1)) || (place2[0] == (place1[0] + 1) && place2[1] == (place1[1] + 1)) || (place2[0] == (place1[0] - 1) && place2[1] == place1[1] || (place2[0] == (place1[0] + 1) && place2[1] == place1[1] ) || (place2[0] == (place1[0] - 1) && place2[1] == (place1[1] - 1)) || (place2[0] == (place1[0] + 1) && place2[1] == (place1[1] - 1)) || (place2[1] == (place1[1] - 1) && place2[0] == place1[0])))
	{
		int ret = dont_interapt(place1, place2, board);
		return ret;
	}
	else // he doesn't have the skills move for the move that got.
	{
		return INVALID_MOVE_INT;
	}
}


/*
private function that Was intended to help the main function of check move by 
check if the move is eat or just regular move.
Input: src location, dst location, board to work with him.
Output:
0 for VALID_PLAY_INT - can do the step.
1 for EAT - can do the step + eat opponent.
*/
int King::dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE])
{
	int srcIndex1 = src[0] - 97;
	int srcIndex2 = src[1] - 49;
	srcIndex2 = INDEX_CONVERT - srcIndex2;
	int dstIndex1 = dst[0] - 97;
	int dstIndex2 = dst[1] - 49;
	dstIndex2 = INDEX_CONVERT - dstIndex2;
	if (board[dstIndex2][dstIndex1] != '#')
	{
		return EAT;
	}
	return VALID_PLAY_INT;
}
