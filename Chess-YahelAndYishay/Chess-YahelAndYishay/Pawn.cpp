#include "Pawn.h"
#include "piece.h"
#include "define_it.h"
#include <string>
#include <iostream>

// Constructor for the Pawn
Pawn::Pawn():piece()
{
	
}

// virtual destructor for the Pawn
Pawn::~Pawn()
{
}

/*
Function will check if this pawn can do the move that got
by check if he can do it with his movement skills and if no one interrupt him.
Input: move to check and the board that help us to see things and check the move.
Output:
6 for INVALID_MOVE_INT - can't move this step.
or what ret got from the help function dont_interapt
*/
int Pawn::can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]) {
	std::string place1 = "";
	place1 = place1 + move[0] + move[1];
	std::string place2 = "";
	place2 = place2 + move[2] + move[3];
	int ret = 0;
		
	if (place2[0] == place1[0])//moving
	{
		if (this->getColor() == "BLACK")//checking color
		{
			if (place2[1] == (place1[1] - 1))
			{
				ret = dont_interapt(move,board,PAWN_MOVE1);	
				return ret;
			}
			else if (get_first_move() == true && place2[1] == (place1[1] - 2))
			{
				ret = dont_interapt(move, board, PAWN_MOVE2);	
				return ret;
			}
			else
			{
				return INVALID_MOVE_INT; // he doesn't have the skills move for the move that got.
			}

		}
		else
		{
			if (place2[1] == (place1[1] + 1))
			{
				ret = dont_interapt(move, board, PAWN_MOVE1);
				return ret;
			}
			else if (get_first_move() == true && place2[1] == (place1[1] + 2))
			{
				ret = dont_interapt(move, board, PAWN_MOVE2);
				return ret;
			}
			else
			{
				return INVALID_MOVE_INT; // he doesn't have the skills move for the move that got.
			}
		}
	}
	else//eating
	{
		if (this->getColor() == "BLACK")//when eating you need to check if you dont eat one of your own pieces
		{
			if (place2[1] == (place1[1] - 1) && (place2[0] == ((place1[0] + 1)) || place2[0] == (place1[0] - 1)))
			{
				
				ret = dont_interapt(move,board,PAWN_EAT);
				if (ret == 0)
				{							
					return EAT;
				}
				return ret;
			}
			else
			{
				return INVALID_MOVE_INT; // he doesn't have the skills move for the move that got.
			}
		}
		else
		{
			if (place2[1] == (place1[1] + 1) && (place2[0] == ((place1[0] + 1)) || place2[0] == (place1[0] - 1)))
			{
				ret = dont_interapt(move, board, PAWN_EAT);
				if (ret == VALID_PLAY_INT)
				{
							return EAT;
				}
				return ret;
			}
			else
			{
				return INVALID_MOVE_INT; // he doesn't have the skills move for the move that got.
			}
		}
	}
	return VALID_PLAY_INT;
}



/*
private function that Was intended to help the main function of check move by try to catch if other piece
that your or not, interupt your moving because they stand in your way.
*/
int Pawn::dont_interapt(std::string move, char board[BOARD_SIZE][BOARD_SIZE], int eatOrMove)
{//0 == oneStep \ 1 == twoSteps \ 2 == eat
	std::string src = "";
	std::string dst = "";
	src = src + move[0] + move[1];
	dst = dst + move[2] + move[3];
	int srcIndex1 = src[0] - MIN_VALUE_TABLE2;
	int srcIndex2 = src[1] - MIN_VALUE_TABLE1;
	int dstIndex1 = dst[0] - MIN_VALUE_TABLE2;
	int dstIndex2 = dst[1] - MIN_VALUE_TABLE1;
	if (eatOrMove == 0)//one step
	{
		if (board[7 - dstIndex2][dstIndex1] != '#')
		{
			return INVALID_DESTINATION_INT;
		}
		else
		{
			return VALID_PLAY_INT;
		}
	}
	else if (eatOrMove == 1)//two steps
	{
		if ((board[INDEX_CONVERT - dstIndex2][dstIndex1] != VACANCY_PLACE || board[INDEX_CONVERT - dstIndex2 + 1][dstIndex1] != VACANCY_PLACE) && (board[INDEX_CONVERT - dstIndex2][dstIndex1] != VACANCY_PLACE || board[INDEX_CONVERT - dstIndex2 - 1][dstIndex1] != VACANCY_PLACE))
		{
			return INVALID_DESTINATION_INT;
		}
		else
		{
			return VALID_PLAY_INT;
		}
	}
	else//eat
	{
		char check = board[INDEX_CONVERT - dstIndex2][dstIndex1];
		if (check != VACANCY_PLACE)
		{
			return VALID_PLAY_INT;
		}
		else
		{
			return INVALID_MOVE_INT;
		}
	}
}
