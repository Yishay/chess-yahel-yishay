#pragma once
#include <string>
#include "piece.h"
#include "Board.h"
#include "define_it.h"

class Knight:public piece
{
public:
	Knight();
	virtual ~Knight();
	int can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]);
private:
	int dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE]);
};

