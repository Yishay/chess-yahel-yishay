#pragma once
#include "piece.h"
#include <string>
#include "define_it.h"

class King:public piece
{
public:
	King();
	virtual ~King();
	int can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]);
private:
	int dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE]);
};

