#pragma once
#include <string>
#include "define_it.h"

// abstract class for any piece in the game
class piece
{
public:
	piece();
	virtual ~piece();
	virtual int can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]) = 0; // pure function.
	// getters:
	std::string getPlace();
	std::string getColor();
	bool get_first_move() const;
	// setters:
	void setPlace(std::string place);
	void setColor(std::string color);
	void set_first_move(bool b);
private:
	std::string place;
	std::string color;
	bool first_move;
};

