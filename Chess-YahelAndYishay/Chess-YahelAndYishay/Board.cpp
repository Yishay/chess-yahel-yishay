#include "Board.h"
#include "Game.h"
#include <string>
#include "Player.h"
#include "piece.h"
#include <iostream>
#include "define_it.h"

//  Constructor for the board -> create board with 2 players with 16 pieces each one and initialized all
Board::Board()
{
	player1 = new Player(WHITE); // white
	player2 = new Player(BLACK); // black
}

// destructor for the board -> delete his 2 players with 16 pieces each one and then the board himself.
Board::~Board()
{
	 this->player1->~Player();
	 this->player2->~Player();
}

/*
function will get the game board the turn of who play and the move to do.
and return if the move is valid by the board and the skill moves (0 - valid or 1 - valid + do chess)
or invalid (6 - invalid or 4 - invalid, get a chess).
*/
int Board::check_move(std::string move, char turn, char game_board[BOARD_SIZE][BOARD_SIZE])
{
	int i = 0, j = 0;
	char temp_game_board[BOARD_SIZE][BOARD_SIZE];
	for (i = 0; i < BOARD_SIZE; i++) // do temp board before change 
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			temp_game_board[i][j] = game_board[i][j];
		}
	}

	int sourceIndexes[2];
	int destIndexes[2];
	char source_in;
	char dest_in;

	sourceIndexes[1] = move[0] - MIN_VALUE_TABLE2; //lines it's like y value
	sourceIndexes[0] = 7 - (move[1] - MIN_VALUE_TABLE1); // rows its like x value 
	destIndexes[1] = move[2] - MIN_VALUE_TABLE2; //lines it's like y value
	destIndexes[0] = 7 - (move[3] - MIN_VALUE_TABLE1); // rows its like x value 
	source_in = game_board[sourceIndexes[0]][sourceIndexes[1]];
	dest_in = game_board[destIndexes[0]][destIndexes[1]];

	bool eating = false;
	std::string src = "";
	src = src + move[0] + move[1];
	std::string dst = "";
	dst = dst + move[2] + move[3];

	piece& s = find_type(move, turn); // get the type of the player that should move
	int ret = s.can_it_move(move, game_board); // check if the player have skill moves to do it.

	// now ret can be 10 for eating + valid move, 0 for valid move, 6 for invalid move (the piece can't do it)
	if (ret == EAT) // eating + valid move
	{
		ret = VALID_PLAY_INT;
		s.setPlace(dst);
		would_remove(move, turn);
		eating = true;
	}
	else if (ret == VALID_PLAY_INT) // valid move
	{
		s.setPlace(dst);
	}
	bool real_first_move = false;
	if (ret == VALID_PLAY_INT) // if there is valid move we should check if we do or got a chess from the move
	{
		if (s.get_first_move() == true)
		{
			real_first_move = true;
		}
		s.set_first_move(false);
		temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = VACANCY_PLACE;
		temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;

		ret = check_chess(turn, temp_game_board);

		if (ret == VALID_PLAY_INT || ret == VALID_PLAY_CHESS_INT) // move seccessed
		{
			if (eating == true) // move passed and we eat enemy piece
			{
				remove_piece(move, turn);
			}
		}
		else // error
		{
			s.setPlace(src); // was error return the move
			if (eating == true)
			{
				revive_piece(move, turn);
			}
			if (real_first_move == true)
			{
				s.set_first_move(true);
			}
		}
	}
	return ret;
}

/*
function will get move and turn that get us player with piece that must be would_remove and the move was passed.
so we should to removed him by changed his place to "" and make him dissapper until the end of the game
*/
void Board::remove_piece(std::string move, char turn)
{
	std::string m = "##";

	if (turn == WHITE_C)
	{
		piece& s = (*player2).find_pices_in_player(m);
		s.setPlace("");
	}
	else
	{
		piece& s = (*player1).find_pices_in_player(m);
		s.setPlace("");
	}
}

/*
function get move and turn that help us to get the enemy piece the would removed (and then mark him by change his place to ##).
why we need her:
if player want to move to enemy piece place (eat him) and the move passed before. (invalid eat)
we dissaper him in the begin of the check and then make sure that this move isn't make a chess 
to himself(the attacking player) because if it was we should to return the player that was should to be eated.
and in the end of the check move we know if he is for return or remove.
*/
void Board::would_remove(std::string move, char turn)
{
	std::string m = "  ";
	m[0] = move[2];
	m[1] = move[3];

	if (turn == WHITE_C)
	{
		piece& s = (*player2).find_pices_in_player(m);
		s.setPlace("##");
	}
	else
	{
		piece& s = (*player1).find_pices_in_player(m);
		s.setPlace("##");
	}
}

/*
function get move and turn that help us to get the enemy piece the would removed (and then return his place instead the temp "##").
function will revive to the game player that would be removed but  his attacker get a chess so he can't eat.
*/
void Board::revive_piece(std::string move, char turn)
{
	std::string m = "  ";
	m[0] = move[2];
	m[1] = move[3];

	if (turn == WHITE_C)
	{
		piece& s = (*player2).find_pices_in_player("##");
		s.setPlace(m);
	}
	else
	{
		piece& s = (*player1).find_pices_in_player("##");
		s.setPlace(m);
	}
}

/*
function will return a type of piece by the turn and the movement (one piece from the src player!)
*/
piece& Board::find_type(std::string move, char turn)
{
	if (turn == WHITE_C)
	{
		return (*player1).find_pices_in_player(move);
	}
	else
	{
		return (*player2).find_pices_in_player(move);
	}
}


/*
function will get a turn of who play and the temp game_board we created.
the function will check chesses and then returned:
0 for vaild play but not any chess got in this move.
1 for vaild play that make a chess to the enemy player.
4 for valid play that make a chess to the player himself ===== invalid play.
*/
int Board::check_chess(char turn, char game_board[BOARD_SIZE][BOARD_SIZE])
{
	int return_value = VALID_PLAY_INT;
	char move[5];
	move[4] = NULL;
	std::string toCheck;
	Player* fplayer1 = player1;
	Player* fplayer2 = player2;
	std::string  k1 = player1->getPlace(INDEX_KING_WHITE); // white
	std::string k2 = player2->getPlace(INDEX_KING_BLACK); // black
	bool white_chess = false;
	bool black_chess = false;
	int i = 0;


	// move the destination of the move the place of the white king
	move[2] = k1[0];
	move[3] = k1[1];
	for (i = 0; i < NUM_OF_PIECES; i++) // check if the white player got a chess 
	{
		toCheck = player2->getPlace(i);
		if (toCheck != "" && toCheck != "##") // the piece is alive
		{
			move[0] = toCheck[0];
			move[1] = toCheck[1];
			return_value = player2->getPiece(i)->can_it_move(move, game_board);
			if (return_value == VALID_PLAY_INT || return_value == EAT) // player2 success to chess player1
			{
				white_chess = true;
			}
		}
	}

	// move the destination of the move the place of the black king
	move[2] = k2[0];
	move[3] = k2[1];
	for (i = 0; i < NUM_OF_PIECES; i++) // check if the black player got a chess 
	{
		toCheck = player1->getPlace(i);
		if (toCheck != "" && toCheck != "##") // the piece is alive
		{
			move[0] = toCheck[0];
			move[1] = toCheck[1];
			return_value = player1->getPiece(i)->can_it_move(move, game_board);
			if (return_value == VALID_PLAY_INT || return_value == EAT) // player2 success to chess player1
			{
				black_chess = true;
			}
		}
	}

	if ((turn == WHITE_C && white_chess) || (turn != WHITE_C && black_chess)) // the color that play make a chess to himself
	{
		return CHESS_DANGER_INT;
	}
	else if ((turn == WHITE_C && black_chess) || (turn != WHITE_C && white_chess)) // the color that play make a chess to the opponent
	{
		return VALID_PLAY_CHESS_INT;
	}
	else // no any chess
	{
		return VALID_PLAY_INT;
	}
}