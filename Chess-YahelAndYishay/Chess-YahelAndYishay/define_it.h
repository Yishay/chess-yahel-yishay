#pragma once
#define PORT 8080 
#define NUM_OF_PAWN 8
#define NUM_OF_PIECES 16
#define WHITE 0
#define BLACK 1
#define WHITE_C '0'
#define BLACK_C '1'

#define MIN_VALUE_TABLE1 '1'
#define MAX_VALUE_TABLE1 '8'
#define MIN_VALUE_TABLE2 'a'
#define MAX_VALUE_TABLE2 'h'
#define VACANCY_PLACE '#'

#define EAT 10

#define VALID_PLAY '0' // valid movement
#define VALID_PLAY_INT 0

#define VALID_CHESS '1' // valid movement of chess
#define VALID_PLAY_CHESS_INT 1

#define INVALID_SOURCE '2' // invalid source place

#define INVALID_DESTINATION '3' // invalid destination place (there is a piece of the player there)
#define INVALID_DESTINATION_INT 3

#define CHESS_DANGER '4' // if the piece move will be a chess to the player who play. (chess to yourself)
#define CHESS_DANGER_INT 4

#define INVALID_INDEX '5' // the index are invalids

#define INVALID_MOVE '6' // invalid move (by skill move of piece or if someone interrupt him)
#define INVALID_MOVE_INT 6

#define	SAME_PLACE '7' // move to yourself location  (not move)
#define	SAME_PLACE_INT 7

#define INDEX_KING_WHITE 3 // king index in whith player pieces array
#define INDEX_KING_BLACK 11 // king index in black player pieces array

#define INDEX_CONVERT 7
#define PAWN_MOVE1 0
#define PAWN_MOVE2 1
#define PAWN_EAT 2
#define QUEEN_STRAIGHT 0
#define QUEEN_NOT_STRAIGHT 1
#define BOARD_SIZE 8
#define HATZRACHA '9'