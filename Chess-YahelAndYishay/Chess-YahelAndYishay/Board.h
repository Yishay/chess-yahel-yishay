#pragma once
#include "Player.h"
#include <string>
#include "define_it.h"
class Board
{
public:
	Board();
	virtual ~Board();
	int check_move(std::string move, char turn, char game_board[BOARD_SIZE][BOARD_SIZE]);
	piece& find_type(std::string move, char turn);
	void remove_piece(std::string move, char turn);
	void would_remove(std::string move, char turn);
	void revive_piece(std::string move, char turn);
	int check_chess(char turn, char game_board[BOARD_SIZE][BOARD_SIZE]);
private:
	Player * player1;
	Player * player2;
	std::string turn;
};
