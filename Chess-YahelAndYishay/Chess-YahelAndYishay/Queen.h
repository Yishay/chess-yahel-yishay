#pragma once
#include "Board.h"
#include <string>
#include "piece.h"
#include "define_it.h"

class Queen:public piece
{
public:
	Queen();
	virtual ~Queen();
	int can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]);
private:
	int dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE],int num);

};

