#include "Rook.h"
#include "piece.h"
#include <string>
#include <iostream>
#include "define_it.h"

// Constructor for the Rook
Rook::Rook() :piece()
{
}

// virtual destructor for the Rook
Rook::~Rook()
{
}

/*
Function will check if this rook can do the move that got
by check if he can do it with his movement skills and if no one interrupt him.
Input: move to check and the board that help us to see things and check the move.
Output:
6 for INVALID_MOVE_INT - can't move this step.
or what ret got from the help function dont_interapt
*/
int Rook::can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]) {
	std::string place1 = ""; // src
	place1 = place1 + move[0] + move[1];
	std::string place2 = ""; // dst
	place2 = place2 + move[2] + move[3];
	int ret = VALID_PLAY_INT;
	if (place1[0] == place2[0] || place1[1] == place2[1])// moving -- move on one line or column
	{
		ret = dont_interapt(place1, place2, board);
		return ret;
	}
	else // he doesn't have the skills move for the move that got.
	{
		return INVALID_MOVE_INT;
	}
}

/*
private function that Was intended to help the main function of check move by try to catch if other piece
that your or not, interupt your moving because they stand in your way.
Input: src location, dst location, board to work with him.
Output:
6 for INVALID_MOVE_INT - can't move this step.
0 for VALID_PLAY_INT - can do the step.
1 for EAT - can do the step + eat opponent.
*/
int Rook::dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE])
{ // 6 == interapt / 0 == no interapt / 10 == no interapt + eating
	int sourceIndexes[2];
	int destIndexes[2];
	sourceIndexes[1] = src[0] - MIN_VALUE_TABLE2; //lines it's like y value
	sourceIndexes[0] = INDEX_CONVERT - (src[1] - MIN_VALUE_TABLE1); // rows its like x value 
	destIndexes[1] = dst[0] - MIN_VALUE_TABLE2; //lines it's like y value
	destIndexes[0] = INDEX_CONVERT - (dst[1] - MIN_VALUE_TABLE1); // rows its like x value 
	int i = 0;
	if (sourceIndexes[1] == destIndexes[1]) // movement on x
	{
		if (destIndexes[0] > sourceIndexes[0])
		{
			for (i = 1; i <= (destIndexes[0] - sourceIndexes[0] - 1); i++)
			{
				if (board[sourceIndexes[0] + i][sourceIndexes[1]] != VACANCY_PLACE)
				{
					return INVALID_MOVE_INT; // interapt
				}
			}
		}
		else // sourceIndexes[0] > destIndexes[0]
		{
			for (i = 1; i <= (sourceIndexes[0] - destIndexes[0] - 1); i++)
			{
				if (board[sourceIndexes[0] - i][sourceIndexes[1]] != VACANCY_PLACE)
				{
					return INVALID_MOVE_INT; // interapt
				}
			}
		}
	}
	else // src[1] == dst[1] -- movement on y
	{
		if (destIndexes[1] > sourceIndexes[1])
		{
			for (i = 1; i <= (destIndexes[1] - sourceIndexes[1] - 1); i++)
			{
				if (board[sourceIndexes[0]][sourceIndexes[1] + i] != VACANCY_PLACE)
				{
					return INVALID_MOVE_INT; // interapt
				}
			}
		}
		else // sourceIndexes[1] > destIndexes[1]
		{
			for (i = 1; i <= (sourceIndexes[1] - destIndexes[1] - 1); i++)
			{
				if (board[sourceIndexes[0]][sourceIndexes[1] - i] != VACANCY_PLACE)
				{
					return INVALID_MOVE_INT; // interapt
				}
			}
		}
	}
	if (board[destIndexes[0]][destIndexes[1]] != '#')
	{
		return EAT; // no interapt + eating
	}
	return VALID_PLAY_INT; // no interapt
}
