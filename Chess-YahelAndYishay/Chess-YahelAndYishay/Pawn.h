#pragma once
#include<string>
#include"piece.h"
#include "Board.h"
#include "define_it.h"

class Pawn:public piece
{
public:
	Pawn();
	virtual ~Pawn();
	int can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]);
private:
	int dont_interapt(std::string move, char board[BOARD_SIZE][BOARD_SIZE], int eatOrMove);
};

