#include "Queen.h"
#include "Board.h"
#include <string>
#include"piece.h"
#include "define_it.h"

// Constructor for the Queen
Queen::Queen() : piece()
{
}

// virtual destructor for the Queen
Queen::~Queen()
{

}

/*
Function will check if this queen can do the move that got
by check if he can do it with his movement skills and if no one interrupt him.
Input: move to check and the board that help us to see things and check the move.
Output:
6 for INVALID_MOVE_INT - can't move this step.
or what ret got from the help function dont_interapt
*/
int Queen::can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE])
{
	std::string src = ""; // src
	src = src + move[0] + move[1];
	std::string dst = ""; // dst
	dst = dst + move[2] + move[3];
	int ret = 0, sub1, sub2;
	if (src[0] == dst[0] || src[1] == dst[1]) // moving like rook
	{
		ret = dont_interapt(src, dst, board, QUEEN_STRAIGHT);
		return ret;
	}
	else // moving like bishop or invalid moving
	{
		if (src[0] > dst[0])
		{
			sub1 = src[0] - dst[0];
		}
		else
		{
			sub1 = dst[0] - src[0];
		}

		if (src[1] > dst[1])
		{
			sub2 = src[1] - dst[1];
		}
		else
		{
			sub2 = dst[1] - src[1];
		}

		if (sub1 == sub2) // Diagonal shift
		{
			ret = dont_interapt(src, dst, board,QUEEN_NOT_STRAIGHT);
			return ret;
		}
		else // she doesn't have the skills move for the move that got.
		{
			ret = INVALID_MOVE_INT;
			return ret;
		}
	}
}

/*
private function that Was intended to help the main function of check move by try to catch if other piece
that your or not, interupt your moving because they stand in your way.
Input: src location, dst location, board to work with him, num to know if check like a bishop or rook.
Output:
6 for INVALID_MOVE_INT - can't move this step.
0 for VALID_PLAY_INT - can do the step.
1 for EAT - can do the step + eat opponent.
*/
int Queen::dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE],int num)//taking the check move of the bishop and the rook creating the check move of the QUEEN
{ // 6 == interapt / 0 == no interapt / 10 == no interapt + eating
	int sourceIndexes[2];
	int destIndexes[2];
	sourceIndexes[1] = src[0] - MIN_VALUE_TABLE2; //lines it's like y value
	sourceIndexes[0] = INDEX_CONVERT - (src[1] - MIN_VALUE_TABLE1); // rows its like x value 
	destIndexes[1] = dst[0] - MIN_VALUE_TABLE2; //lines it's like y value
	destIndexes[0] = INDEX_CONVERT - (dst[1] - MIN_VALUE_TABLE1); // rows its like x value 
	int i = 0;
	if (num == 0) {
		if (sourceIndexes[1] == destIndexes[1]) // movement on x
		{
			if (destIndexes[0] > sourceIndexes[0])
			{
				for (i = 1; i <= (destIndexes[0] - sourceIndexes[0] - 1); i++)
				{
					if (board[sourceIndexes[0] + i][sourceIndexes[1]] != VACANCY_PLACE)
					{
						return INVALID_MOVE_INT; // interapt
					}
				}
			}
			else // sourceIndexes[0] > destIndexes[0]
			{
				for (i = 1; i <= (sourceIndexes[0] - destIndexes[0] - 1); i++)
				{
					if (board[sourceIndexes[0] - i][sourceIndexes[1]] != VACANCY_PLACE)
					{
						return INVALID_MOVE_INT; // interapt
					}
				}
			}
		}
		else // src[1] == dst[1] -- movement on y
		{
			if (destIndexes[1] > sourceIndexes[1])
			{
				for (i = 1; i <= (destIndexes[1] - sourceIndexes[1] - 1); i++)
				{
					if (board[sourceIndexes[0]][sourceIndexes[1] + i] != VACANCY_PLACE)
					{
						return INVALID_MOVE_INT; // interapt
					}
				}
			}
			else // sourceIndexes[1] > destIndexes[1]
			{
				for (i = 1; i <= (sourceIndexes[1] - destIndexes[1] - 1); i++)
				{
					if (board[sourceIndexes[0]][sourceIndexes[1] - i] != VACANCY_PLACE)
					{
						return INVALID_MOVE_INT; // interapt
					}
				}
			}
		}
		if (board[destIndexes[0]][destIndexes[1]] != '#')
		{
			return EAT; // no interapt + eating
		}
		return VALID_PLAY_INT; // no interapt
	}
	else
	{
		if (sourceIndexes[0] > destIndexes[0] && sourceIndexes[1] > destIndexes[1]) // source bigger than destination
		{
			for (i = 1; sourceIndexes[1] - i > destIndexes[1]; i++)
			{
				if (board[sourceIndexes[0] - i][sourceIndexes[1] - i] != '#')
				{
					return INVALID_MOVE_INT;
				}
			}
		}
		else if (sourceIndexes[0] < destIndexes[0] && sourceIndexes[1] < destIndexes[1]) // destination bigger than source
		{
			for (i = 1; sourceIndexes[1] + i < destIndexes[1]; i++)
			{
				if (board[sourceIndexes[0] + i][sourceIndexes[1] + i] != '#') // tere is another piece in the way
				{
					return INVALID_MOVE_INT;
				}
			}
		}
		else if (sourceIndexes[0] < destIndexes[0] && sourceIndexes[1] > destIndexes[1])
		{
			for (i = 1; sourceIndexes[0] + i < destIndexes[0]; i++)
			{
				if (board[sourceIndexes[0] + i][sourceIndexes[1] - i] != '#') // tere is another piece in the way
				{
					return INVALID_MOVE_INT;
				}
			}
		}
		else // (sourceIndexes[0] > destIndexes[0] && sourceIndexes[1] < destIndexes[1])
		{
			for (i = 1; sourceIndexes[1] + i < destIndexes[1]; i++)
			{
				if (board[sourceIndexes[0] - i][sourceIndexes[1] + i] != '#') // tere is another piece in the way
				{
					return INVALID_MOVE_INT;
				}
			}
		}
		// if we pass it there is no interapt
		if (board[destIndexes[0]][destIndexes[1]] != '#') // kill opponent piece
		{
			return EAT;
		}
		// else
		return VALID_PLAY_INT; // regular move 
	}
}
