#pragma once
#include<string>
#include"piece.h"
#include "Board.h"
#include "define_it.h"

class Bishop :public piece
{
public:
	Bishop();
	virtual ~Bishop();
	int can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE]);
private:
	int dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE]);
};