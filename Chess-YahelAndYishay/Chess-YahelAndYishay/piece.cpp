#include "piece.h"
#include "define_it.h"


// Constructor for any piece
piece::piece()
{
	this->first_move = true;
}

// virtual destructor for any piece
piece::~piece()
{
}

// function will return boolien - if the piece didn't do move in the game return true (his first move) and false for else. 
bool piece::get_first_move() const
{
	return this->first_move;
}

// function that set the first_move of the piece to the boolien parameter that got (b)
void piece::set_first_move(bool b)
{
	this->first_move = b;
}

// function that set the color of the piece to the string parameter that got (color)
void piece::setColor(std::string color)
{
	this->color = color;
}

// function that set the place of the piece to the string parameter that got (place)
void piece::setPlace(std::string place)
{
	this->place = place;
}

// function will return a string of the piece place. 
std::string piece::getPlace()
{
	return this->place;
}

// function will return a string of the piece color. 
std::string piece::getColor()
{
	return this->color;
}
