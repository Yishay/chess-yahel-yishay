#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Game.h"
#include "define_it.h"


using namespace std;
int main()
{
	srand(time_t(NULL));// srand functions for make sure random() function is correct


	Pipe p; // Pipe is class that communicate with the board graphics
	bool isConnect = p.connect(); 

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return 0;
		}
	}


	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"); // this is the starting string for a game

	p.sendMessageToGraphics(msgToGraphics);   // send the board string
	
	Game game(msgToGraphics); // Game is the upper class that run's the game
	char newMove[5] = { 0 };
	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();///reciving
	//TODO: should recive from the socket
	char send_to_graphics[2];
	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)

		// OUR CODE:
		// check the move from graphics and return char '0'-'7' answer + null to the graphics
		send_to_graphics[0] = game.check_move(msgFromGraphics);
		send_to_graphics[1] = NULL;
		game.print_status(msgFromGraphics, send_to_graphics[0]);
		/*every time you see the comment to HATZRACHA it means that the program checks the 4 moves that possible
		for it. if our move was one of this 4 we are cheking if the move is possible and the return answer/
		every time there is a big simular code
		*/
		if (send_to_graphics[0] == HATZRACHA)//in case of 
		{
			if (msgFromGraphics == "a1d1" || msgFromGraphics == "d1a1")
			{
				newMove[0] = 'b';
				newMove[1] = '1';
				newMove[2] = 'c';
				newMove[3] = '1';
			}
			else if (msgFromGraphics == "h1d1" || msgFromGraphics == "d1h1")
			{
				newMove[0] = 'f';
				newMove[1] = '1';
				newMove[2] = 'e';
				newMove[3] = '1';
			}
			else if (msgFromGraphics == "a8d8" || msgFromGraphics == "d8a8")
			{
				newMove[0] = 'b';
				newMove[1] = '8';
				newMove[2] = 'c';
				newMove[3] = '8';
			}
			else if (msgFromGraphics == "h8d8" || msgFromGraphics == "d8h8")
			{
				newMove[0] = 'f';
				newMove[1] = '8';
				newMove[2] = 'e';
				newMove[3] = '8';
			}


			strcpy_s(msgToGraphics, newMove);
			p.sendMessageToGraphics(msgToGraphics);
		}
		else
		{
			strcpy_s(msgToGraphics, send_to_graphics); // msgToGraphics should contain the result of the operation											   // return result to graphics		
			p.sendMessageToGraphics(msgToGraphics); ///sending
			//TODO: send to socket
		}
		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();///reciving again
	}
	game.~Game();
	p.close();
}
