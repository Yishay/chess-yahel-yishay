#include "Knight.h"
#include"piece.h"
#include "define_it.h"
#include <string>
#include <iostream>

// Constructor for the Knight
Knight::Knight():piece()
{
}

// virtual destructor for the Knight
Knight::~Knight()
{
}

/*
Function will check if this knight can do the move that got
by check if he can do it with his movement skills and if no one interrupt him.
Input: move to check and the board that help us to see things and check the move.
Output:
6 for INVALID_MOVE_INT - can't move this step.
or what ret got from the help function dont_interapt
*/
int Knight::can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE])
{
	std::string src = ""; // src
	src = src + move[0] + move[1];
	std::string dst = ""; // dst
	dst = dst + move[2] + move[3];
	
	// if he can move:
	if (((dst[0] == src[0]+1) || (dst[0] == src[0]-1)) && (((dst[1] == src[1] + 2) || (dst[1] == src[1] - 2))) || ((dst[0] == src[0]+2) || (dst[0] == src[0]-2)) && (((dst[1] == src[1] + 1) || (dst[1] == src[1] - 1))))
	{
		int ret = dont_interapt(src, dst, board);
		return ret;
	}
	else // he doesn't have the skills move for the move that got.
	{
		return INVALID_MOVE_INT;
	}
}

/*
private function that Was intended to help the main function of check move by
check if the move is eat or just regular move.
Input: src location, dst location, board to work with him.
Output:
0 for VALID_PLAY_INT - can do the step.
1 for EAT - can do the step + eat opponent.
*/
int Knight::dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE])
{
	int sourceIndexes[2];
	int destIndexes[2];
	sourceIndexes[1] = src[0] - MIN_VALUE_TABLE2; //lines it's like y value
	sourceIndexes[0] = INDEX_CONVERT - (src[1] - MIN_VALUE_TABLE1); // rows its like x value 
	destIndexes[1] = dst[0] - MIN_VALUE_TABLE2; //lines it's like y value
	destIndexes[0] = INDEX_CONVERT - (dst[1] - MIN_VALUE_TABLE1); // rows its like x value 
	if (board[destIndexes[0]][destIndexes[1]] != '#')
	{
		return EAT;
	}
	return VALID_PLAY_INT;
}
