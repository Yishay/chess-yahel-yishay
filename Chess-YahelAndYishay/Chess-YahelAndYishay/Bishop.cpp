#include "Bishop.h"
#include "piece.h"
#include <string>
#include <iostream>
#include "define_it.h"


// Constructor for the Bishop
Bishop::Bishop() :piece()
{
}
// virtual destructor for the Bishop
Bishop::~Bishop()
{
}


/*
Function will check if this bishop can do the move that got
by check if he can do it with his movement skills and if no one interrupt him.
Input: move to check and the board that help us to see things and check the move. 
Output:
6 for INVALID_MOVE_INT - can't move this step.
or what ret got from the help function dont_interapt
*/
int Bishop::can_it_move(std::string move, char board[BOARD_SIZE][BOARD_SIZE])
{
	int ret, sub1, sub2;
	std::string src = "";
	src = src + move[0] + move[1];
	std::string dst = "";
	dst = dst + move[2] + move[3];
	if (src[0] > dst[0])
	{
		sub1 = src[0] - dst[0];
	}
	else
	{
		sub1 = dst[0] - src[0];
	}

	if (src[1] > dst[1])
	{
		sub2 = src[1] - dst[1];
	}
	else
	{
		sub2 = dst[1] - src[1];
	}

	if (sub1 == sub2) // Diagonal shift
	{
		ret = dont_interapt(src, dst, board);
		return ret;
	}
	// else -- he doesn't have the skills move for the move that got.
	return INVALID_MOVE_INT;
}

/*
private function that Was intended to help the main function of check move by try to catch if other piece 
that your or not, interupt your moving because they stand in your way.
Input: src location, dst location, board to work with him. 
Output:
6 for INVALID_MOVE_INT - can't move this step.
0 for VALID_PLAY_INT - can do the step.
1 for EAT - can do the step + eat opponent.
*/
int Bishop::dont_interapt(std::string src, std::string dst, char board[BOARD_SIZE][BOARD_SIZE])
{
	int sourceIndexes[2];
	int destIndexes[2];
	sourceIndexes[1] = src[0] - MIN_VALUE_TABLE2; //lines it's like y value
	sourceIndexes[0] = INDEX_CONVERT - (src[1] - MIN_VALUE_TABLE1); // rows its like x value 
	destIndexes[1] = dst[0] - MIN_VALUE_TABLE2; //lines it's like y value
	destIndexes[0] = INDEX_CONVERT - (dst[1] - MIN_VALUE_TABLE1); // rows its like x value 
	int i = 0;
	if (sourceIndexes[0] > destIndexes[0] && sourceIndexes[1] > destIndexes[1]) // source bigger than destination
	{
		for (i = 1; sourceIndexes[1] - i > destIndexes[1]; i++)
		{
			if (board[sourceIndexes[0] - i][sourceIndexes[1] - i] != '#') // tere is another piece in the way - interrupt
			{
				return INVALID_MOVE_INT;
			}
		}
	}
	else if (sourceIndexes[0] < destIndexes[0] && sourceIndexes[1] < destIndexes[1]) // destination bigger than source
	{
		for (i = 1; sourceIndexes[1] + i < destIndexes[1]; i++)
		{
			if (board[sourceIndexes[0] + i][sourceIndexes[1] + i] != '#') // tere is another piece in the way - interrupt
			{
				return INVALID_MOVE_INT;
			}
		}
	}
	else if (sourceIndexes[0] < destIndexes[0] && sourceIndexes[1] > destIndexes[1]) // they both bigger. (source in index 1 and destination in index 0) 
	{
		for (i = 1; sourceIndexes[0] + i < destIndexes[0]; i++)
		{
			if (board[sourceIndexes[0] + i][sourceIndexes[1] - i] != '#') // tere is another piece in the way - interrupt
			{
				return INVALID_MOVE_INT;
			}
		}
	}
	else // (sourceIndexes[0] > destIndexes[0] && sourceIndexes[1] < destIndexes[1]) - - - - they both bigger. (source in index 0 and destination in index 1) 
	{
		for (i = 1; sourceIndexes[1] + i < destIndexes[1]; i++)
		{
			if (board[sourceIndexes[0] - i][sourceIndexes[1] + i] != '#') // tere is another piece in the way - interrupt
			{
				return INVALID_MOVE_INT;
			}
		}
	}
	// if we pass it there is no interrupt
	if (board[destIndexes[0]][destIndexes[1]] != '#') // kill opponent piece
	{
		return EAT;
	}
	// else
	return VALID_PLAY_INT; // regular move 
}
