#include "Game.h"
#include <iostream>
#include <string>
#include "define_it.h"

// Constructor for the game -> create game,board, 2 players, 32 pieces and initialized all
// and also make a 2D char game_board that help us organize the game.
Game::Game(char * gameb)
{
	this->black_chess_check = false;
	this->white_chess_check = false;
	int i = 0, j = 0;
	for (i = 0; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			this->game_board[i][j] = gameb[i * 8 + j];
		}
	}
	this->who_play = gameb[64];
	this->printBoard();
}

// destructor for the game -> delete all the board, 2 players, 32 pieces and then the game himself.
Game::~Game()
{
	this->board.~Board();
}

// function will set the if there is chess on black or chess on white influence of the boolien that got -c (if there is chess ot not) and the turn
void Game::setChessCheck(bool c, char turn)
{
	if (turn == '0')
	{
		this->white_chess_check = c;
	}
	else
	{
		this->black_chess_check = c;
	}
}


// function will return if there is a chess right now by get the turn and see if the player got a chess 
bool Game::getChessCheck(char turn) const
{
	if (turn == '0')
	{
		return this->white_chess_check;
	}
	else
	{
		return this->black_chess_check;
	}
}

/*
function will get a string of move (like "a2a4") and return to the source and then to the grapics 
if the move is ok 0 or 1 and 2-7 if the move is invalid.
she check if the move isn't to the same place the index are valids and if piece doesn't move to another team piece
and also if the piece have the skill moves to do it, moreover if there isn't chess to yourself after the move(in the board class)...
*/
char Game::check_move(std::string move)
{
	int return_value = 0;
	int i = 0;
	char source[2];
	char dest[2];
	int sourceIndexes[2];
	int destIndexes[2];
	char source_in;
	char dest_in;
	if (move.length() != 4) // 4 letters
	{
		return INVALID_INDEX;
	}
	//checks if the move is on the board
	for (i = 0; i < move.length(); i++)
	{
		if (i % 2 == 0 && move[i] >= MIN_VALUE_TABLE2 && move[i] <= MAX_VALUE_TABLE2)
		{
		}
		else if (i % 2 == 1 && move[i] >= MIN_VALUE_TABLE1 && move[i] <= MAX_VALUE_TABLE1)
		{
		}
		else
		{
			return INVALID_INDEX;
		}
	}
	// now we know that the index are valid
	source[0] = move[0];
	source[1] = move[1];
	dest[0] = move[2];
	dest[1] = move[3];

	//checks if the dest and sourc are the same
	if (source[0] == dest[0] && source[1] == dest[1])
	{
		return SAME_PLACE;
	}

	// check if the is problem of invalid source place a2a4
	sourceIndexes[1] = move[0] - MIN_VALUE_TABLE2; //lines it's like y value
	sourceIndexes[0] = INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1); // rows its like x value 
	destIndexes[1] = move[2] - MIN_VALUE_TABLE2; //lines it's like y value
	destIndexes[0] = INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1); // rows its like x value 
	source_in = this->game_board[sourceIndexes[0]][sourceIndexes[1]];
	dest_in = this->game_board[destIndexes[0]][destIndexes[1]];
	if (this->who_play == WHITE_C && 'A' <= source_in && source_in <= 'Z')
	{
	}
	else if (this->who_play != WHITE_C && 'a' <= source_in && source_in <= 'z')
	{
	}
	else
	{
		return INVALID_SOURCE;
	}
	// check if the is problem of invalid destion place	
	if ((this->game_board[sourceIndexes[0]][sourceIndexes[1]] != '#'
		&& this->game_board[destIndexes[0]][destIndexes[1]] != '#'
		&& ((this->game_board[sourceIndexes[0]][sourceIndexes[1]] >= 'A'
			&& this->game_board[sourceIndexes[0]][sourceIndexes[1]] <= 'Z'
			&& this->game_board[destIndexes[0]][destIndexes[1]] >= 'A'
			&& this->game_board[destIndexes[0]][destIndexes[1]] <= 'Z')
			|| (this->game_board[sourceIndexes[0]][sourceIndexes[1]] >= 'a'
				&& this->game_board[sourceIndexes[0]][sourceIndexes[1]] <= 'z'
				&& this->game_board[destIndexes[0]][destIndexes[1]] >= 'a'
				&& this->game_board[destIndexes[0]][destIndexes[1]] <= 'z'))))
	{
		return INVALID_MOVE;
	}
		piece& s = getPlayer(source, who_play);//source player
		piece& d = getPlayer(dest, who_play);//destination player
		std::string castlingIndex = "  ";
		std::string source_b = s.getPlace();
		std::string dest_b = d.getPlace();
		int castlingRet = 0;
		char temp_game_board[BOARD_SIZE][BOARD_SIZE];
		//sets the temp board to the real board
		for (int k = 0; k < BOARD_SIZE; k++)
		{
			for (int l = 0; l < BOARD_SIZE; l++)
			{
				temp_game_board[k][l] = this->game_board[k][l];
			}
		}
		// check if the move is is a Castling (HATZRACHA)
		if (s.get_first_move() == true && d.get_first_move() == true)
		{
			if (s.getColor() == "WHITE" && white_chess_check == false)
			{
				//checks if its white hatracha
				if (move == "d1a1")
				{
					move = "a1c1";
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = d.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("b1");
						d.setPlace("c1");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							s.set_first_move(false);
							d.set_first_move(false);
							move = "d1a1";
					
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'K';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'R';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
						}
					}
					move = "d1a1";
				}
				else if (move == "d1h1")
				{
					move = "h1e1";
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = d.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("f1");
						d.setPlace("e1");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							s.set_first_move(false);
							d.set_first_move(false);
							move = "d1h1";
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'K';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'R';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE; 
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
						}
					}
					move = "d1h1";
				}
				else if (move == "h1d1")
				{
					move[2] = 'e';
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = s.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("e1");
						d.setPlace("f1");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							s.set_first_move(false);
							d.set_first_move(false);
							move[2] = 'd';
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'K';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'R';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE; 
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
						}
					}
					move[2] = 'd';
				}
				else if (move == "a1d1")
				{
					move[2] = 'c';
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = s.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("c1");
						d.setPlace("b1");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							s.set_first_move(false);
							d.set_first_move(false);
							move[2] = 'd';
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'R';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'K';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE; 
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
						}
					}
					move[2] = 'd';
				}
			}
			else if (black_chess_check == false)
			{
			//checks if its black hatracha
				if (move == "a8d8")
				{
					move[2] = 'c';
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = s.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("c8");
						d.setPlace("b8");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							s.set_first_move(false);
							d.set_first_move(false);
							move[2] = 'd';
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'r';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'k';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE; 
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
						}
					}
					move[2] = 'd';
				}
				else if (move == "d8a8")
				{
					move = "a8c8";
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = d.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("b8");
						d.setPlace("c8");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							s.set_first_move(false);
							d.set_first_move(false);
							move = "d8a8";
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'k';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'r';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE; 
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
						}
					}
					move = "d8a8";
				}
				else if (move == "h8d8")
				{
					move[2] = 'e';
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = s.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("e8");
						d.setPlace("f8");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							move[2] = 'd';
							s.set_first_move(false);
							d.set_first_move(false);
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'r';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'k';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE; 
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
							
						}
					}
					move[2] = 'd';
				}
				else if (move == "d8h8")
				{
					move = "h8e8";
					temp_game_board[sourceIndexes[0]][sourceIndexes[1]] = dest_in;
					temp_game_board[destIndexes[0]][destIndexes[1]] = source_in;
					castlingRet = d.can_it_move(move, game_board);
					if (castlingRet == VALID_PLAY_INT)
					{
						s.setPlace("f8");
						d.setPlace("e8");
						castlingRet = this->board.check_chess(who_play, temp_game_board);
						if (castlingRet == VALID_PLAY_INT || castlingRet == VALID_PLAY_CHESS_INT)
						{
							s.set_first_move(false);
							d.set_first_move(false);
							move = "d8h8";
							game_board[INDEX_CONVERT - (s.getPlace()[1] - MIN_VALUE_TABLE1)][s.getPlace()[0] - MIN_VALUE_TABLE2] = 'K';
							game_board[INDEX_CONVERT - (d.getPlace()[1] - MIN_VALUE_TABLE1)][d.getPlace()[0] - MIN_VALUE_TABLE2] = 'R';
							game_board[INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1)][move[0] - MIN_VALUE_TABLE2] = VACANCY_PLACE;
							game_board[INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1)][move[2] - MIN_VALUE_TABLE2] = VACANCY_PLACE; 
							if (who_play == WHITE_C)
							{
								who_play = BLACK_C;
							}
							else
							{
								who_play = WHITE_C;
							}
							return HATZRACHA;
						}
						else
						{
							s.setPlace(source_b);
							d.setPlace(dest_b);
							s.set_first_move(true);
							d.set_first_move(true);
							
						}
					}
					move = "d8h8";
				}
			}
		}
	

	//checks if the destintion is correct
	if (this->who_play == WHITE_C && 'A' <= dest_in && dest_in <= 'Z')
	{
		return INVALID_DESTINATION;
	}
	else if (this->who_play != WHITE_C && 'a' <= dest_in && dest_in <= 'z')
	{
		return INVALID_DESTINATION;
	}
	//gets the output of the check move if its regular move or check move
	return_value = this->board.check_move(move, this->who_play, this->game_board);
	if (return_value == VALID_PLAY_INT)
	{
		setChessCheck(false,who_play);		
	}
	else if(return_value == VALID_PLAY_CHESS_INT)
	{
		if (who_play == WHITE_C)
		{
			setChessCheck(true, BLACK_C);
		}
		else
		{
			setChessCheck(true, WHITE_C);
		}
	}
	//sets the turn for the right color
	if (return_value == VALID_PLAY - '0' || return_value == VALID_CHESS - '0')
	{
		if (who_play == WHITE_C)
		{
			who_play = BLACK_C;
		}
		else
		{
			who_play = WHITE_C;
		}
		this->game_board[sourceIndexes[0]][sourceIndexes[1]] = VACANCY_PLACE;
		this->game_board[destIndexes[0]][destIndexes[1]] = source_in;
	}
	return (char)(return_value + '0');
}


//  function will return the player we asking for b the move and turn
piece& Game::getPlayer(std::string move, char turn)
{
	move[2] = 0;
	return this->board.find_type(move, turn);
}


// function will print to the screen the game details and the board by the function printBoard
void Game::print_status(std::string move, char return_msg)
{
	//std::cout << "Receives " << move.length() << "bytes; Message: \"" << move << "\"" << std::endl;
	int Sindex[2];
	int Dindex[2];
	Sindex[1] = move[0] - MIN_VALUE_TABLE2; //lines it's like y value
	Sindex[0] = INDEX_CONVERT - (move[1] - MIN_VALUE_TABLE1); // rows its like x value 
	Dindex[1] = move[2] - MIN_VALUE_TABLE2; //lines it's like y value
	Dindex[0] = INDEX_CONVERT - (move[3] - MIN_VALUE_TABLE1); // rows its like x value 
	std::cout << "Move from: " << Sindex[0] << ", " << Sindex[1] << "To " << Dindex[0] << ", " << Dindex[1] << std::endl;
	//std::cout << "Sends 2 bytes; Message: \"" << return_msg << "\"" << std::endl;
	//this->printBoard();
}

// function will print the board to the screen
void Game::printBoard()
{
	int i = 0, j = 0;
	for (i = 0; i < BOARD_SIZE; i++)
	{
		for (j = 0; j < BOARD_SIZE; j++)
		{
			std::cout << " " << this->game_board[i][j];
		}
		std::cout << std::endl;
	}
}