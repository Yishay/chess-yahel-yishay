#pragma once
#include "Board.h"
#include "define_it.h"

class Game
{
public:
	Game(char * gameb);
	virtual ~Game();
	char check_move(std::string move);
	void print_status(std::string move, char return_msg);
	piece& getPlayer(std::string move, char turn);
	void setChessCheck(bool c, char turn);
	bool getChessCheck(char turn) const;
private:
	void printBoard();
	Board board;
	char game_board[8][8];
	char who_play; // 0 white , other = black
	bool black_chess_check;
	bool white_chess_check;
};
