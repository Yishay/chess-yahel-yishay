#pragma once
#include "piece.h"
#include <string>
#include "define_it.h"

class Player
{
public:
	Player(int color);
	virtual ~Player();
	piece& find_pices_in_player(std::string move);
	std::string getPlace(int n) const;
	piece* getPiece(int n);
protected:
	piece * p[16];
	std::string color;
};
