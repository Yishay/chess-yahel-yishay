﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SQLite;
namespace chessGraphics
{
    public partial class Signup : Form
    {
        SQLiteConnection sqlite_conn;
        User user;
        public Signup(SQLiteConnection client)
        {
            InitializeComponent();
            this.sqlite_conn = client;
        }



        private void Button1_Click(object sender, EventArgs e)
        {
            //sets new user according to the input boxes
            this.user = new User(this.username.Text, this.password.Text, this.pet.Text);

            //verify the vars
            if (this.user.getPassword() != this.verify.Text)
            {
                MessageBox.Show("password and verification not equals!");
                return;
            }
            //verify empty value
            else if (this.user.getUsername() == "" || this.user.getPassword() == "" || this.user.getPet() == "")
            {
                MessageBox.Show("Empty Value!");
                return;
            }
            // if we dont get an empty string it meens that the user already exist
            if (ReadUserData(sqlite_conn, this.user.getUsername()) != "")
            {
                MessageBox.Show("name alerady in system");
                return;
            }
            InsertNewUser(sqlite_conn, user);// inserting the user in the system
            MessageBox.Show("you are now in system:)");

        }
        /*funcion for inserting new user
         input: connection, username, password, email
         output: None*/
        private void InsertNewUser(SQLiteConnection conn, User user)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO users(username,password,email) VALUES(" +
            "'" + user.getUsername() + "','" + user.getPassword() + "','" + user.getPet() + "');";
            sqlite_cmd.ExecuteNonQuery();
        }
        /*funcion for reading info about user
         input: connection, username
         output: username, password, email*/
        private string ReadUserData(SQLiteConnection conn, string username)
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM users  WHERE username LIKE '" + username + "'";

            sqlite_datareader = sqlite_cmd.ExecuteReader(); // Execut the command
            string ans = "";

            if (sqlite_datareader.Read())
            {
                //uses in , as a delimiter
                //MessageBox.Show(sqlite_datareader["username"].ToString());
                ans = sqlite_datareader["password"].ToString() + "," + sqlite_datareader["email"].ToString();
            }
            else// if no user found return empty string
            {
                ans = "";
                //throw (new Exception("No user found!"));
            }
            return ans;
        }

 
    }
    
}
