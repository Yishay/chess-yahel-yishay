﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chessGraphics
{
    /*Square class is made to make the working with moving the tool easier*/
    public class Square
    {
        int _colIndex;
        int _rowIndex;
        
        public Square(int row, int col)
        {
            _rowIndex = row;
            _colIndex = col;
        }
        public Square(string toSquare)
        {
            _colIndex = toSquare[1] - 'a';
            _rowIndex = 8 - (toSquare[0] - 48);
            _rowIndex.ToString();
            //return Convert.ToChar('a' + _colIndex).ToString() + (8 - _rowIndex).ToString();
        }
        //getters
        public int Row
        {
            get { return _rowIndex; }
        }

        public int Col
        {
            get { return _colIndex; }
        }

        public override string ToString()
        {
            return Convert.ToChar('a' + _colIndex).ToString() + (8 - _rowIndex).ToString();
        }
    }
}
