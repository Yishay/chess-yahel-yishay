﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace chessGraphics
{
    public partial class Form1 : Form
    {
        private NetworkStream stream;
        private TcpClient client;
        private bool turn;

        private Square srcSquare;
        private Square dstSquare;
        private Square hsrc1;
        private Square hsrc2;
        private Square hdst1;
        private Square hdst2;
        private pipe enginePipe;
        Button[,] matBoard; // its a 8*8 matrix that represnt all the tools

        bool isCurPlWhite = true;
        bool isGameOver = false;

        const int BOARD_SIZE = 8;

        public Form1()
        {
            InitializeComponent();
        }

        public Form1(System.Net.Sockets.TcpClient client, bool turn)
        {   
            InitializeComponent();
            this.client = client;
            this.stream = client.GetStream();
            this.turn = turn;
        }

        /*
         function start everything like ctor
         for the whole program. funtions start the enginePipe class, the class is used to make 
         connection with the logic of the chess
         */
        private void initForm()
        {
            enginePipe.connect();

            Invoke((MethodInvoker)delegate {

                lblWaiting.Visible = false;
                lblCurrentPlayer.Visible = true;
                label1.Visible = true;



                string s = enginePipe.getEngineMessage();
                //check if the starting string is acccording to protocoll
                if (s.Length != (BOARD_SIZE * BOARD_SIZE + 1))
                {
                    MessageBox.Show("The length of the board's string is not according the PROTOCOL");
                    this.Close();

                }
                else
                {
                    isCurPlWhite = (s[s.Length - 1] == '0');
                    paintBoard(s);
                }

            });

        }

        Thread connectionThread;//the thread is made for the program can run while speaking to the logic
        private void Form1_Load(object sender, EventArgs e)
        {
            enginePipe = new pipe();
            //this.Show();

            //MessageBox.Show("Press OK to start waiting for engine to connect...");
            connectionThread = new Thread(initForm);
            connectionThread.Start();
            connectionThread.IsBackground = true;

            // opens engine
            System.Diagnostics.Process.Start("Chess-YahelAndYishay.exe");

            //initForm();


        }

        //the switch case is to get every tool set like the protocol
        //upcase means while and lower case is black
        //returns Image of the tool
        Image getImageBySign(char sign)
        {
            
            switch (sign)
            {
                case 'q':
                    return Properties.Resources.q_black;
                case 'Q':
                    return Properties.Resources.q_white;
                case 'k':
                    return Properties.Resources.k_black;
                case 'K':
                    return Properties.Resources.k_white;
                case 'p':
                    return Properties.Resources.p_black;
                case 'P':
                    return Properties.Resources.p_white;
                case 'r':
                    return Properties.Resources.r_black;
                case 'R':
                    return Properties.Resources.r_white;
                case 'n':
                    return Properties.Resources.n_black;
                case 'N':
                    return Properties.Resources.n_white;
                case 'b':
                    return Properties.Resources.b_black;
                case 'B':
                    return Properties.Resources.b_white;
                case '#':
                    return null;
                default:
                    return Properties.Resources.x;

            }
        }

        /*one of the base functions
         take the board and paint it nicely and sets the vars when mouse clicks and hover.
         */
        private void paintBoard(string board)
        {
            int i, j, z = 0;

            matBoard = new Button[BOARD_SIZE, BOARD_SIZE];

            btnBoard.FlatAppearance.MouseOverBackColor = Color.LightGray;

            Button newBtn;
            Point pnt;

            int currentWidth = btnBoard.Location.X;
            int currentHeight = btnBoard.Location.Y;

            bool isColBlack = true;
            bool isRowBlack = true;

            this.SuspendLayout();

            lblCurrentPlayer.Text = isCurPlWhite ? "White" : "Black";

            for (i = 0; i < BOARD_SIZE; i++)
            {
                currentWidth = btnBoard.Location.X;
                isColBlack = isRowBlack;
                //sets all the tools as buttons with texuras
                for (j = 0; j < BOARD_SIZE; j++)
                {
                    newBtn = new Button();
                    matBoard[i, j] = newBtn;
                    //desides on the look of the tools borders
                    newBtn.FlatAppearance.MouseOverBackColor = btnBoard.FlatAppearance.MouseOverBackColor;
                    newBtn.BackColor = isColBlack ? Color.Gray : Color.White;
                    newBtn.FlatAppearance.BorderColor = btnBoard.FlatAppearance.BorderColor;
                    newBtn.FlatAppearance.BorderSize = btnBoard.FlatAppearance.BorderSize;
                    newBtn.FlatStyle = btnBoard.FlatStyle;

                    newBtn.Size = new System.Drawing.Size(btnBoard.Width, btnBoard.Height);
                    newBtn.Tag = new Square(i, j);
                    pnt = new Point(currentWidth, currentHeight);
                    newBtn.Location = pnt;
                    newBtn.BackgroundImageLayout = ImageLayout.Stretch;

                    newBtn.BackgroundImage = getImageBySign(board[z]);

                    newBtn.Click += lastlbl_Click;

                    Controls.Add(newBtn);


                    currentWidth += btnBoard.Width;
                    isColBlack = !isColBlack;
                    z++;

                }

                isRowBlack = !isRowBlack;
                currentHeight += btnBoard.Height;

            }

            Controls.Remove(btnBoard);
            this.ResumeLayout(false);
        }

        /*
         funtion is made for the player can  play while pipe is running
             */
        void lastlbl_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (srcSquare != null)
            {
                // unselected
                if (matBoard[srcSquare.Row, srcSquare.Col] == b)
                {

                    matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.Blue;
                    srcSquare = null;
                }
                else
                {
                    dstSquare = (Square)b.Tag;
                    matBoard[dstSquare.Row, dstSquare.Col].FlatAppearance.BorderColor = Color.DarkGreen;

                    //if its this computers' turn
                    if (this.turn)
                    {
                        Thread t = new Thread(playMove);
                        t.Start();
                    }
                    else
                    {
                        string tmp = SocketAssets.ReciveMsg(this.stream, this.client);
                        Tuple<Square, Square> tuple = parseSocketToMove(tmp);
                        moveTool(tuple.Item1, tuple.Item2);
                    }
                    //changing turn flag
                    this.turn = !this.turn;

                    //   t.IsBackground = true;
                    //playMove();
                }
            }
            else
            {
                srcSquare = (Square)b.Tag;
                matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.DarkGreen;
            }

        }

        // messages should be according the protocol.
        // index is the message number in the protocol
        string[] messages =  {
            "Valid move",
            "Valid move - you made chess",
            "Invalid move - not your player",
            "Invalid move - destination is not free",
            "Invalid move - chess wil occure",
            "Invalid move - out of bounds",
            "Invalid move - illegeal movement with piece",
            "Invalid move - source and dest are equal",
            "Game over - check mate",
            "Unknown message"
            };

       
        //converting from binary to string
        string convertEngineToText(string m)
        {
            int res;
            bool b = int.TryParse(m, out res);
            if (m.Length >= 4)
            {
                return m;
            }
        
         
            if (!b || res< 0 || res >= messages.Length){
                return messages[messages.Length - 1];
            }
            return messages[res];
        }
        

        /*
         the main function of the program.
         print all the info on the board(turn,moves, msg from logic)
             */
        void playMove()
        {
            if (isGameOver)
                return;
            try
            {
                 Invoke((MethodInvoker)delegate {


                     lblEngineCalc.Visible = true;
            
                     lblMove.Text = string.Format("Move from {0} to {1}", srcSquare, dstSquare);
                    lblMove.Visible = true;
                    //lblEngineCalc.Invalidate();
            
                    label2.Visible = false;
                    lblResult.Visible = false;

                    this.Refresh();

                     // should send pipe to engine
                     enginePipe.sendEngineMove(srcSquare.ToString() + dstSquare.ToString());


                     // should get pipe from engine
                    string m = enginePipe.getEngineMessage();


                     // if made chess (code 1), enable "Mate" button
                     if (m == "1")
                     {
                         button1.Visible = true;
                     }

                    if (!enginePipe.isConnected())
                    {
                        MessageBox.Show("Connection to engine has lost. Bye bye.");
                        this.Close();
                        return;
                    }

                    string res = convertEngineToText(m);

                    if (res.ToLower().StartsWith("game over"))
                    {
                        isGameOver = true;
                    }
                    else if (res.ToLower().StartsWith("valid"))
                    {
                         //sending approved move to other pc
                         SocketAssets.SendMsg(this.stream, this.client, srcSquare.ToString() + dstSquare.ToString());
                         //Tuple<Square, Square> t = parseSocketToMove("2e4e");
                         //moving piece in local pc
                         moveTool(srcSquare, dstSquare);//e2,e4
                    }
                     //in case of HATZRACHA
                     else if (res.Length == 4)
                     {
                         isCurPlWhite = !isCurPlWhite;
                         lblCurrentPlayer.Text = isCurPlWhite ? "White" : "Black";
                         //in case of HATZRACHA
                         if (res == "b1c1")
                         {
                             //7,1 \ 7,2
                             if ((dstSquare.Col == 0 || dstSquare.Col == 7))
                             {
                                 matBoard[7, 1].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[7, 2].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                             else
                             {
                                 matBoard[7, 2].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[7, 1].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                         }
                         //in case of HATZRACHA
                         else if (res == "f1e1")
                         {
                             //7,4 \ 7,5 
                             if (!(dstSquare.Col == 0 || dstSquare.Col == 7))
                             {
                                 matBoard[7, 4].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[7, 5].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                             else
                             {
                                 matBoard[7, 5].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[7, 4].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                         }
                         //in case of HATZRACHA
                         else if (res == "b8c8")
                         {
                             //0,1 \ 0,2
                             if ((dstSquare.Col == 0 || dstSquare.Col == 7))
                             {
                                 matBoard[0, 1].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[0, 2].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                             else
                             {
                                 matBoard[0, 2].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[0, 1].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                         }
                         //in case of HATZRACHA
                         else if (res == "f8e8")
                         {
                             //0,4 \ 0,5
                             if (!(dstSquare.Col == 0 || dstSquare.Col == 7))
                             {
                                 matBoard[0, 4].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[0, 5].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                             else
                             {
                                 matBoard[0, 5].BackgroundImage = matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage;
                                 matBoard[srcSquare.Row, srcSquare.Col].BackgroundImage = null;
                                 matBoard[0, 4].BackgroundImage = matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage;
                                 matBoard[dstSquare.Row, dstSquare.Col].BackgroundImage = null;
                             }
                         }
                         matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.Blue;
                         matBoard[dstSquare.Row, dstSquare.Col].FlatAppearance.BorderColor = Color.Blue;
                     }
                    lblEngineCalc.Visible = false;
                    lblResult.Text = string.Format("{0}", res);
                    lblResult.Visible = true;
                    label2.Visible = true;
                    this.Refresh();
                 });
                }
                catch
                {
                    
                }
                finally
                {
                    Invoke((MethodInvoker)delegate
                    {
                        if (srcSquare != null)
                            matBoard[srcSquare.Row, srcSquare.Col].FlatAppearance.BorderColor = Color.Blue;

                        if (dstSquare != null)
                            matBoard[dstSquare.Row, dstSquare.Col].FlatAppearance.BorderColor = Color.Blue;

                        dstSquare = null;
                        srcSquare = null;

                    });
                }

        }

        /* should get a string like this "a2a4" and parse it to 2 squares,
           and returning a tuple with src and dst squares.
        */
        Tuple<Square, Square> parseSocketToMove(string m)
        {
            Square src = new Square(m[0].ToString() + m[1].ToString());

            Square dst = new Square(m[2].ToString() + m[3].ToString());

            return Tuple.Create(src, dst);
        }

        /*
         Square is a class we build to help use the colums and lines
         the ctor of square is Square(int row, int col)
         the function moveTool is uses to move tool on GUI board
             */
        void moveTool(Square fsrc, Square fdst)
        {
            isCurPlWhite = !isCurPlWhite;
            lblCurrentPlayer.Text = isCurPlWhite ? "White" : "Black";
            //changing the tool image on the board
            matBoard[fdst.Row,fdst.Col].BackgroundImage = matBoard[fsrc.Row, fsrc.Col].BackgroundImage;
            matBoard[fsrc.Row, fsrc.Col].BackgroundImage = null;

            matBoard[fsrc.Row, fsrc.Col].FlatAppearance.BorderColor = Color.Blue;
            matBoard[fdst.Row, fdst.Col].FlatAppearance.BorderColor = Color.Blue;
        }

        //destructor for the form
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            enginePipe.sendEngineMove("quit");
            enginePipe.close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Mate! you won the game!!");
            Application.Exit();
        }
    }
}
