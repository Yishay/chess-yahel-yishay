﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SQLite;
namespace chessGraphics
{
    public partial class MainWIndow : Form
    {
        SQLiteConnection client;
        public MainWIndow()
        {
            InitializeComponent();
            string path = "users.db";
            string pathAdd = "Data Source=" + path + "; Version = 3; New = True; Compress = True; ";
            this.client = new SQLiteConnection(pathAdd);
            // Open the connection:
            this.client.Open();
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login login = new Login(this.client);
            login.ShowDialog();
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Signup signup = new Signup(this.client);
            signup.ShowDialog();
            this.Close();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            DialogResult result =  MessageBox.Show("Do you know how to play chess?", "game rules", MessageBoxButtons.YesNoCancel); ;
            //sends the user to learn chess rules
            if (result == DialogResult.No)
            {
                System.Diagnostics.Process.Start("https://www.ichess.co.il/article.html#!id=8");
            }
        }
    }
}
