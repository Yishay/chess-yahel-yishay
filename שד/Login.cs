﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SQLite;
namespace chessGraphics
{
    public partial class Login : Form
    {
        SQLiteConnection sqlite_conn;
        public Login(SQLiteConnection client)
        {
            InitializeComponent();
            this.sqlite_conn = client;
        }
        //main Enter button
        private void Button1_Click(object sender, EventArgs e)
        {
            string username = this.username.Text;
            string password = this.password.Text;
            if (username == "" || password == "")//checks if there is password and username
            {
                MessageBox.Show("Empty Value!");
                return;
            }
            string ans = "";
            ans = ReadUserData(this.sqlite_conn, username);//send message to the db
            //check for the username
            if (ans == "")
            {
                MessageBox.Show("user not found!");
                return;
            }
            //check for the password
            if (ans.Split(',')[0] != password)
            {
                MessageBox.Show("wrong password!");
                return;
            }
            this.Hide();
            FirstForm first = new FirstForm();
            first.ShowDialog();
            this.Close();
        }
        //forgot password
        private void Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Forgotpassword forgotpassword = new Forgotpassword(this.sqlite_conn);
            forgotpassword.ShowDialog();
            this.Close();
        }
        /*funcion for inserting new user
        input: connection, username, password, email
        output: None*/
        private void InsertNewUser(SQLiteConnection conn, User user)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO users(username,password,email) VALUES(" +
            "'" + user.getUsername() + "','" + user.getPassword() + "','" + user.getPet() + "');";
            sqlite_cmd.ExecuteNonQuery();
        }
        /*function for reading info about user
         input: connection, username
         output: username, password, email*/
        private string ReadUserData(SQLiteConnection conn, string username)
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM users  WHERE username LIKE '" + username + "'";

            sqlite_datareader = sqlite_cmd.ExecuteReader(); // Execut the command
            string ans = "";

            if (sqlite_datareader.Read())
            {
                //uses in , as a delimiter
                //MessageBox.Show(sqlite_datareader["username"].ToString());
                ans = sqlite_datareader["password"].ToString() + "," + sqlite_datareader["pet"].ToString();
            }
            else// if no user found return empty string
            {
                ans = "";
                //throw (new Exception("No user found!"));
            }
            return ans;
        }
    }
}
