﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace chessGraphics
{
    public partial class FirstForm : Form
    {
        private Socket _socket;
        public FirstForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            HostGame tmp = new HostGame();
            tmp.Show();
            tmp.Refresh();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            JoinGame tmp = new JoinGame();
            tmp.ShowDialog();
            this.Close();
        }
    }
}
