﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace chessGraphics
{
    public partial class HostGame : Form
    {

        private System.Net.Sockets.TcpListener _listener;
        private System.Net.Sockets.TcpClient _client;

        public HostGame()
        {
            InitializeComponent();
        }

        private void HostGame_Load(object sender, EventArgs e)
        {
            _listener = new System.Net.Sockets.TcpListener(8888);
            this.Shown += new System.EventHandler(this.HostGame_Shown);
        }

        private void HostGame_Shown(Object sender, EventArgs e)
        {
            string hostname  = Dns.GetHostName();
            LabelIP.Text = "Your ip is: " + Dns.GetHostByName(hostname).AddressList[0].ToString();
            this.Refresh();
            AcceptClient();
        }

        private void AcceptClient()
        {
            this._listener.Start();
            this._client = _listener.AcceptTcpClient();

            //NetworkStream stream = this._client.GetStream();
            //MessageBox.Show(SocketAssets.ReciveMsg(stream, this._client));
            //SocketAssets.SendMsg(stream, this._client, "This is server");

            this.Hide();
            Form1 tmp = new Form1(this._client, true);
            tmp.Show();
            tmp.Refresh();
            this.Close();
        }
    }
}
