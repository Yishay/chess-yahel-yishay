﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Text;

namespace chessGraphics
{

    class SocketAssets
    {

        public static string ReciveMsg(NetworkStream stream, TcpClient client)
        {
            //---get the incoming data through a network stream---
            byte[] buffer = new byte[client.ReceiveBufferSize];

            //---read incoming stream---
            int bytesRead = stream.Read(buffer, 0, client.ReceiveBufferSize);

            //---convert the data received into a string---
            string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);


            return dataReceived;
        }

        public static void SendMsg(NetworkStream stream, TcpClient client, string msg)
        {
            byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(msg);

            //---send the text---
            stream.Write(bytesToSend, 0, bytesToSend.Length);
        }

    }
}
