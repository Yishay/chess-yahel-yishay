﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace chessGraphics
{
    public partial class JoinGame : Form
    {

        System.Net.Sockets.TcpClient _client;
        public JoinGame()
        {
            InitializeComponent();
        }

        private void JoinGame_Load(object sender, EventArgs e)
        {
            _client = new System.Net.Sockets.TcpClient();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            _client.Connect(textBox1.Text, 8888);

            //NetworkStream stream = this._client.GetStream();
            //SocketAssets.SendMsg(stream, this._client, "This is client");
            //MessageBox.Show(SocketAssets.ReciveMsg(stream, this._client));

            this.Hide();
            Form1 tmp = new Form1(this._client, false);
            tmp.ShowDialog();
            this.Close();

        }
    }
}
