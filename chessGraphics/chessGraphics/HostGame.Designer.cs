﻿namespace chessGraphics
{
    partial class HostGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelIP = new System.Windows.Forms.Label();
            this.WaitLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LabelIP
            // 
            this.LabelIP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelIP.AutoSize = true;
            this.LabelIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.LabelIP.Location = new System.Drawing.Point(234, 149);
            this.LabelIP.Name = "LabelIP";
            this.LabelIP.Size = new System.Drawing.Size(323, 39);
            this.LabelIP.TabIndex = 1;
            this.LabelIP.Text = "Your IP is: 00.0.0.00";
            // 
            // WaitLabel
            // 
            this.WaitLabel.AutoSize = true;
            this.WaitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.WaitLabel.Location = new System.Drawing.Point(121, 238);
            this.WaitLabel.Name = "WaitLabel";
            this.WaitLabel.Size = new System.Drawing.Size(562, 58);
            this.WaitLabel.TabIndex = 2;
            this.WaitLabel.Text = "Waiting for connection...";
            // 
            // HostGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.WaitLabel);
            this.Controls.Add(this.LabelIP);
            this.Name = "HostGame";
            this.Text = "HostGame";
            this.Load += new System.EventHandler(this.HostGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelIP;
        private System.Windows.Forms.Label WaitLabel;
    }
}