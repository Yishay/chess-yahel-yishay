﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SQLite;
namespace chessGraphics
{
    public partial class Forgotpassword : Form
    {
        SQLiteConnection sqlite_conn;
        public Forgotpassword(SQLiteConnection client)
        {
            InitializeComponent();
            this.sqlite_conn = client;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string username = this.username.Text;
            string pet = this.pet.Text;
            string ans = ReadUserData(this.sqlite_conn, username);
            if (ans == "")
            {
                MessageBox.Show("user not found!");
                return;
            }
            if (ans.Split(',')[1] != pet)
            {
                MessageBox.Show("wrong pet!");
                return;
            }
            MessageBox.Show("your password is:"+ ans.Split(',')[0]);
        }
        private string ReadUserData(SQLiteConnection conn, string username)
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM users  WHERE username LIKE '" + username + "'";

            sqlite_datareader = sqlite_cmd.ExecuteReader(); // Execut the command
            string ans = "";

            if (sqlite_datareader.Read())
            {
                //uses in , as a delimiter
                //MessageBox.Show(sqlite_datareader["username"].ToString());
                ans = sqlite_datareader["password"].ToString() + "," + sqlite_datareader["pet"].ToString();
            }
            else// if no user found return empty string
            {
                ans = "";
                //throw (new Exception("No user found!"));
            }
            return ans;
        }
    }
}
