﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data.SQLite;
using System.Diagnostics;
namespace chessLogin
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        SQLiteConnection sqlite_conn;
        public Window1(SQLiteConnection sqlite_conn)
        {
            InitializeComponent();
            this.sqlite_conn = sqlite_conn;
        }
        /*function for enter the game*/
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = this.username.Text;
            string password = this.password.Text;
            if (username == "" || password == "")//checks if there is password and username
            {
                MessageBox.Show("Empty Value!");
                return;
            }
            string ans = "";
            ans = ReadUserData(this.sqlite_conn, username);//send message to the db
            //check for the username
            if (ans == "")
            {
                MessageBox.Show("user not found!");
                return;
            }
            //check for the password
            if (ans.Split(',')[0] != password)
            {
                MessageBox.Show("wrong password!");
                return;
            }
            Process.Start("Chess-YahelAndYishay.exe");


        }
        /*funcion for inserting new user
        input: connection, username, password, email
        output: None*/
        private void InsertNewUser(SQLiteConnection conn, User user)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO users(username,password,email) VALUES(" +
            "'" + user.getUsername() + "','" + user.getPassword() + "','" + user.getPet() + "');";
            sqlite_cmd.ExecuteNonQuery();
        }
        /*function for reading info about user
         input: connection, username
         output: username, password, email*/
        private string ReadUserData(SQLiteConnection conn, string username)
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM users  WHERE username LIKE '" + username + "'";

            sqlite_datareader = sqlite_cmd.ExecuteReader(); // Execut the command
            string ans = "";

            if (sqlite_datareader.Read())
            {
                //uses in , as a delimiter
                //MessageBox.Show(sqlite_datareader["username"].ToString());
                ans = sqlite_datareader["password"].ToString() + "," + sqlite_datareader["pet"].ToString();
            }
            else// if no user found return empty string
            {
                ans = "";
                //throw (new Exception("No user found!"));
            }
            return ans;
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Enter user name and your pet's name:", "forgot password", MessageBoxButton.YesNoCancel);
        }


    }
}
