﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data.SQLite;

namespace chessLogin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SQLiteConnection client;
        public MainWindow()
        {
            InitializeComponent();
            //sets the vars for the db
            string path = "C:\\Users\\user\\source\\repos\\chess-yahel-yishay\\users.db";
            string pathAdd = "Data Source=" + path + "; Version = 3; New = True; Compress = True; ";
            this.client = new SQLiteConnection(pathAdd);
            // Open the connection:
            this.client.Open();
        }
        //login window
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Window1 login = new Window1(this.client);
            this.Visibility = Visibility.Hidden;
            login.Show();
            
        }
        //signup window
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
            Window2 signup = new Window2(this.client);
            this.Visibility = Visibility.Hidden;
            signup.Show();

        }
        //click if the user dosent know how to play
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you know how to play chess?", "Rules", MessageBoxButton.YesNo) == MessageBoxResult.No)
            {
                    System.Diagnostics.Process.Start("https://www.ichess.co.il/article.html#!id=8");
            };
            
        }
    }
}
