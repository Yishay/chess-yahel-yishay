﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chessLogin
{
    class User
    {
        private string username;
        private string password;
        private string pet;
        //the class has info about a user.
        //the class is made to make the information trasioning easier
        public User(string username, string password, string pet)
        {
            this.username = username;
            this.password = password;
            this.pet = pet;
        }
        //getters, our class dosen't require use in setters because we dont wont any change in the vars
        public string getUsername()
        {
            return this.username;
        }
        public string getPassword()
        {
            return this.password;
        }
        public string getPet()
        {
            return this.pet;
        }
    }
}
